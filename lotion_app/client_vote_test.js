
let { post } = require('axios')
let secp = require('secp256k1')
let { sha256, pubkeyToAddress, getTxHash, getSignature } = require('./src/utils.js')


async function main () {
  let tx = {
    type: "vote",
    question: "What is the best cryptocurrency?",
    voterPubkey: pub,
    answer: "bitcoin"
  }

  // sign tx
  let sigHash = getTxHash(tx)
  tx.signature = getSignature(sigHash, priv)

  let res = await post('http://localhost:3001/txs', tx)
  console.log(res.data)
  
  tx = {
    type: "vote",
    question: "What is the best cryptocurrency?",
    voterPubkey: pub2,
    answer: "bitcoin"
  }
  
  // sign tx
  sigHash = getTxHash(tx)
  tx.signature = getSignature(sigHash, priv2)

  res = await post('http://localhost:3001/txs', tx)
  console.log(res.data)
  
  tx = {
    type: "vote",
    question: "What is the best cryptocurrency?",
    voterPubkey: pub3,
    answer: "ether"
  }
  
  // sign tx
  sigHash = getTxHash(tx)
  tx.signature = getSignature(sigHash, priv3)

  res = await post('http://localhost:3001/txs', tx)
  console.log(res.data)
  
}
main()