# Decentralized Q/A Platform

Question and answer platform built on top of lotion blockchain.

# Development Server

* From the root of the project run `./run_backend.sh`. This will start the development server.
* In a seperate terminal window run `./run_frontend.sh`.
* Open `localhost:3000` in a browser page to view the site. Enter username `test` to try out demo.
